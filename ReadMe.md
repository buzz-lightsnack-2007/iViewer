# iViewer
An iFrame wrapper, best to preview files with additional controls

## Features
- [x] gets and displays title of embedded document
- [x] full screen

### For Developers
- [x] works with web code injection
